import time
import allure
from allure_commons.types import Severity

from selenium.webdriver.chrome.webdriver import WebDriver

@allure.title('Onliner iPhone page')
@allure.severity(Severity.TRIVIAL)
def test_onliner():

    with allure.step('Инициализация драйвера'):
            driver = WebDriver(executable_path='E://chromedriver.exe')
            driver.get('https://www.onliner.by')
            assert "Onliner" in driver.page_source

    with allure.step('Проверка заголовка'):
            search_input = driver.find_element_by_class_name('fast-search__input')
            time.sleep(2)
            search_input.send_keys('iPhone')

    with allure.step('Поиск телефона'):
            frame = driver.find_element_by_xpath('//iframe[@class="modal-iframe"]')
            driver.switch_to.frame(frame)
            time.sleep(2)
            all_result = driver.find_elements_by_xpath('//*[@class="result__item result__item_product"]')
            time.sleep(5)

    with allure.step('Проверка пустых значений'):
            if len(all_result) > 0:
                link = all_result[0].find_element_by_xpath('.//*[@class="product__title"]').click()
            else:
                return print('No one obj found')
            assert "Apple iPhone 11 64GB (черный)" in driver.page_source

    with allure.step('Драйвер закрыт'):
            driver.close()
